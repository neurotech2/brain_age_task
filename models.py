import torch
from torch import nn
from torch.nn import functional as F
from torch.nn.utils import weight_norm

import wandb
import pytorch_lightning as pl

import copy


class BrainAgeTemplateModel(pl.LightningModule):

    def __init__(self, hparams):
        super().__init__()

        self.hparams.update(hparams)
        self.save_hyperparameters()

    def configure_optimizers(self):
        optim = torch.optim.Adam(self.parameters(), lr=self.hparams["learning_rate"], weight_decay=self.hparams["weight_decay"])
        scheduler = torch.optim.lr_scheduler.OneCycleLR(optim,
                                                       max_lr=self.hparams["learning_rate"], 
                                                       steps_per_epoch=self.n_batches, epochs=self.hparams["epochs"]
                                                       )

        # optim_dict = {
        #     "optimizer": optim,
        #     "interval":1,
        #     "lr_scheduler": {
        #     "scheduler": scheduler,
        #     }     
        # }
        return [optim], [scheduler]
        
    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x).squeeze()
        loss = F.l1_loss(y_hat, y)
        self.log("loss", loss, on_step=True, on_epoch=True, prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x).squeeze()
        val_loss = F.l1_loss(y_hat, y)
        
        self.log("val loss", val_loss, on_step=True, on_epoch=True, prog_bar=True)
        return val_loss


    def save(self, path):
        print('Saving model... %s' % path)
        torch.save(self, path)




class EEGResidualblock(pl.LightningModule):

    def __init__(self, D_in, D_out, kernel_size, dilation, norm=True):
        super().__init__()
        self.conv1 = nn.Conv2d(D_in, D_out, kernel_size=(1, kernel_size), dilation=dilation, padding="same")
        self.norm1 = nn.LazyBatchNorm2d() if norm else nn.Identity()
        
        self.conv2 = nn.Conv2d(D_out, D_out, kernel_size=(1, kernel_size), dilation=dilation, padding="same")
        self.norm2 = nn.LazyBatchNorm2d() if norm else nn.Identity()

        self.conv3 = nn.Conv2d(D_out, 2*D_out, kernel_size=(1,1))
        self.norm3 = nn.LazyBatchNorm2d() if norm else nn.Identity()
        
        self.act1 = nn.GELU()
        self.act2 = nn.GLU(dim=1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.norm1(x)
        x = self.act1(x)

        x = self.conv2(x)
        x = self.norm2(x)
        x = self.act1(x)

        x = self.conv3(x)
        x = self.norm3(x)
        x = self.act2(x)

        return x



class EEGResnet(BrainAgeTemplateModel):


    def __init__(self, hparams, block=EEGResidualblock):
        super().__init__(hparams)
        
        self.block = block
        self.kernel_size_base = hparams["kernel_size_base"]
        self.n_filters = hparams["n_filters"]
        self.n_layers = hparams["n_layers"]

        self.norm = hparams["norm"]
        
        self.backbone1 = self.make_block(0)
        self.backbone2 = self.make_block(1)
        # self.backbone3 = self.make_block(2)

        # self.backbone = [self.make_block(i) for i in range(self.n_layers)]
        head_modules = [nn.AdaptiveMaxPool2d((1,1)), nn.Flatten(), nn.LazyLinear(1)]
        head_modules = head_modules + [nn.GELU()] if not hparams["centered_output"] else head_modules
        self.head = nn.Sequential(*head_modules)


    def forward(self, x):
        # for block in self.backbone:
        #     x = x + block(x)
        x = self.backbone1(x)
        x = self.backbone2(x)
        # x = self.backbone3(x)
        x = self.head(x)
        return x


    def make_block(self, n_layer):

        if n_layer == 0:
            D_in = 1
            D_out = self.n_filters
        else:
            D_in = D_out = self.n_filters

        dilation_size = 2**((2*n_layer)%5)
        kernel_size = self.kernel_size_base
        norm = self.norm

        return self.block(D_in, D_out, kernel_size, dilation_size, norm=norm)



class DilatedConv1DBlock(pl.LightningModule):

    def __init__(self, D_in, D_out, kernel_size, dilation, norm=True):
        super().__init__()
        self.conv = nn.Conv1d(D_in, D_out, kernel_size, stride=dilation, dilation=dilation, padding=dilation)
        self.norm = nn.LazyBatchNorm1d() if norm else nn.Identity()
        self.act = nn.ReLU()
    
    def forward(self, x):
        x = self.conv(x)
        x = self.norm(x)
        x = self.act(x)
        return x



class DilatedConv1DNet(BrainAgeTemplateModel):

    def __init__(self, hparams, block=DilatedConv1DBlock):
        super().__init__(hparams)
        
        self.block = block
        self.kernel_size_base = hparams["kernel_size_base"]
        self.n_filters = hparams["n_filters"]
        self.n_layers = hparams["n_layers"]
        self.base = hparams["base"]

        self.norm = hparams["norm"]

        
        if not self.does_coverage(length=600):
            print("increase kernel size or number of layers for full coverage")
        else:
            print(f"receptive field is {self.receptive_field()}")
        
        self.backbone = nn.Sequential(*[self.make_block(i) for i in range(self.n_layers)])
        head_modules = [nn.AdaptiveMaxPool1d(1), nn.Flatten(), nn.LazyLinear(1)]
        head_modules = head_modules + [nn.ReLU()] if not hparams["centered_output"] else head_modules
        self.head = nn.Sequential(*head_modules)


    def forward(self, x):
        x = self.backbone(x)
        x = self.head(x)
        return x

    def does_coverage(self, length):
        return 1+(self.kernel_size_base-1)*(self.base**self.n_layers-1)\
            /(self.base -1) >= length

    def receptive_field(self):
        return 1 + (self.kernel_size_base - 1) * (self.base**self.n_layers - 1)/(self.base - 1)


    def make_block(self, n_layer):

        if n_layer == 0:
            D_in = 129
            D_out = self.n_filters
        else:
            D_in = D_out = self.n_filters

        dilation_size = self.base**n_layer
        kernel_size = self.kernel_size_base

        return self.block(D_in, D_out, kernel_size, dilation_size)


class Conv1DBlock(pl.LightningModule):

    def __init__(self, D_in, D_out, kernel_size, norm=True, pool="max", pool_factor=5, dilation=1, p_dropout=None):
        super().__init__()

        self.conv = nn.Conv1d(D_in, D_out, kernel_size, padding="same", dilation=dilation)
        self.norm = nn.LazyBatchNorm1d() if norm else nn.Identity()
        self.act = nn.GELU()
        if pool=="max":
            self.pool = nn.MaxPool1d(kernel_size=pool_factor, stride=pool_factor)
        if pool=="avg":
            self.pool = nn.MaxPool1d(kernel_size=pool_factor, stride=pool_factor)
        self.dropout = nn.Dropout1d(p_dropout) if p_dropout else nn.Identity()

    def forward(self, x):
        x = self.conv(x)
        x = self.norm(x)
        x = self.act(x)
        x = self.pool(x)
        x = self.dropout(x)
        return x




class Conv1DNet(BrainAgeTemplateModel):

    def __init__(self, hparams, block=Conv1DBlock):
        super().__init__(hparams)
        
        self.block = block
        self.kernel_size_base = hparams["kernel_size_base"]
        self.n_filters = hparams["n_filters"]
        self.n_layers = hparams["n_layers"]

        self.pool = hparams["pool"]
        self.pool_factor = hparams["pool_factor"]
        self.norm = hparams["norm"]
        self.dilation = hparams["dilation"]
        self.p_dropout = hparams["p_dropout"]

        self.backbone = nn.Sequential(*[self.make_conv_block(i) for i in range(self.n_layers)])
        head_modules = [nn.AdaptiveMaxPool1d(1), nn.Flatten(), nn.LazyLinear(1)]
        head_modules = head_modules + [nn.GELU()] if not hparams["centered_output"] else head_modules
        self.downsample = nn.GLU(dim=1) if hparams["downsample_chs"] else nn.Identity()
        self.head = nn.Sequential(*head_modules)


    def forward(self, x):
        x = self.backbone(x)
        x = self.downsample(x)
        x = self.head(x)
        return x

    def make_conv_block(self, n_layer):
        
        if n_layer == 0:
            D_in = 129
            D_out = self.n_filters
        else:
            D_in = D_out = self.n_filters

        kernel_size = self.kernel_size_base // (n_layer + 1)
        kernel_size = 3 if kernel_size < 3 else kernel_size

        norm = self.norm
        pool = self.pool
        pool_factor = self.pool_factor
        dilation = self.dilation
        p_dropout = self.p_dropout

        return self.block(D_in, D_out, kernel_size, norm, pool, pool_factor, dilation, p_dropout)
    

class EEGInceptionBlock(pl.LightningModule):

    def __init__(self, n_chs, G_out, D_in, d_multiplier, kernel_base, downsample_factor=1,
                imformat_first=True, imformat_last=False):
        super().__init__()

        self.n_chs = n_chs
        self.G_out = G_out
        self.D_in = D_in
        self.d_multiplier = d_multiplier
        self.kernel_base = kernel_base
        self.downsample_factor = downsample_factor
        
        self.conv1 = nn.Conv2d(D_in, G_out, (1, kernel_base), padding="same")
        self.conv2 = nn.Conv2d(D_in, G_out, 2*kernel_base, padding="same")
        self.conv3 = nn.Conv2d(D_in, G_out, 4*kernel_base, padding="same")

        self.norm1 = nn.LazyBatchNorm1d()
        self.norm2 = nn.LazyBatchNorm1d()

        self.convdw = nn.Conv1d(3*G_out, int(d_multiplier*3*G_out), (1, n_chs), padding="same")
        self.avpool = nn.AvgPool1d(downsample_factor, downsample_factor)

    def forward(self, x):
        x = self.norm1(x)
        b1 = self.conv1(x)
        b2 = self.conv2(x)
        b3 = self.conv3(x)

        x = torch.cat([b1, b2, b3], dim=1)
        x = self.norm2(x)
        x = self.convdw(x)
        x = self.avpool(x)
        return x

import numpy as np

def io_diagnostics(model, composed_transform, input_dim):
    outputs = []
    for i in range(100):
        dummy_input = np.random.normal(loc=10, size=input_dim)
        out = model(composed_transform(dummy_input).unsqueeze(0))
        outputs.append(out)
    
    outputs = torch.tensor(outputs)
    mean_out, std_out = torch.mean(outputs, dim=0), torch.std(outputs, dim=0)
    return mean_out, std_out