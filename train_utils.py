import os

from pytorch_lightning.loggers import WandbLogger
from pytorch_lightning.callbacks import ModelCheckpoint

import wandb
import pytorch_lightning as pl

from data_utils import BrainAgeDataset
from torch.utils.data import DataLoader, ConcatDataset, random_split


def get_kfold_datasets(k, folds, transforms):

    val_dataset = BrainAgeDataset(folds[k], transforms)
    train_folds = [BrainAgeDataset(folds[j], transforms) for j in range(len(folds)) if i!=j]  
    train_dataset = ConcatDataset(train_folds)

    return train_dataset, val_dataset

def train(train_dataset, val_dataset, BrainAgeModel, hparams, experiment_name, trainer_kwargs, lr_tune=False, ckpt_dir=None):
    
    train_dataloader = DataLoader(train_dataset, batch_size=hparams['batch_size'], shuffle=True, drop_last=False)
    val_dataloader = DataLoader(val_dataset, batch_size=hparams['batch_size'], shuffle=False, drop_last=False)

    log_model = False
    if ckpt_dir:
        trainer_kwargs.update({
            "enable_checkpointing": True if ckpt_dir else False
            })
            # "checkpoints": [ModelCheckpoint(dirpath=ckpt_dir, monitor="val loss")]
            # })
        log_model = True

    wandb_logger = WandbLogger(project="brain_age_prediction", group=experiment_name, job_type="eval", log_model='all', entity="tschwarz", save_dir=ckpt_dir)
    os.environ["WANDB_RUN_GROUP"] = "experiment-" + wandb.util.generate_id()
    
    
    trainer = pl.Trainer(max_epochs=hparams["epochs"], logger=wandb_logger, **trainer_kwargs, auto_lr_find="learning_rate")
    model = BrainAgeModel(hparams)
    model.n_batches = len(train_dataloader)

    if lr_tune:
        trainer.tune(model, train_dataloader, val_dataloader)
    
    trainer.fit(model, train_dataloader, val_dataloader)

    wandb.finish()
    
    trainer = None

    return model



def train_with_kfolds(folds, BrainAgeModel, hparams, experiment_name, transforms, target_transforms, trainer_kwargs, lr_tune=True, ckpt_dir=None):

    for i, fold in enumerate(folds):
        
        print(f"==================== fold {i} ==========================")

        val_dataset = BrainAgeDataset(folds[i], transforms)
        train_folds = [BrainAgeDataset(folds[j], transforms) for j in range(len(folds)) if i!=j]  
        train_dataset = ConcatDataset(train_folds)

        train(train_dataset, val_dataset, BrainAgeModel, hparams, experiment_name, transforms, trainer_kwargs, lr_tune, ckpt_dir)