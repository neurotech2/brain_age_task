import pathlib

import numpy as np
import torch

from torch.utils.data import Dataset

def get_subject_dirs(data_dir, subj_df):
    subj_dirs = []
    for subj_id in subj_df["id"]:
        subj_dir = data_dir / str(subj_id)
        subj_dirs.append(subj_dir)
    return subj_dirs

def select_subj_hb(subj_dirs):
    raise NotImplementedError

def select_samples(subj_dirs, max_n_rec, task=None, exist_check=False):

    samples = []
    for path in subj_dirs:
        if task:
            path = path / task
        
        for i in range(max_n_rec):
            f = path / f"{i+1}.npy"
            if not exist_check or f.is_file():
              samples.append(f)
    return samples

#### Datasets #####

class BrainAgeDataset(Dataset):
    
    def __init__(self, data_paths, subj_df, transforms=None, target_transforms=None, local_dir=None, do_save=True):
        
        self.data_paths_remote = data_paths
        self.transforms = transforms
        self.target_transforms = target_transforms

        self.do_save = do_save
        if local_dir:
            self.local_dir = local_dir
        else:
            self.local_dir = pathlib.Path().absolute() # current working dir
        self.data_paths_local = self.get_data_paths_local()
        
        self.subj_df = subj_df


    def __len__(self):
        return len(self.data_paths_remote)

    def __getitem__(self, idx):

        
        try:
            eeg = np.load(self.data_paths_local[idx])
            age = self.subj_df[self.subj_df["id"]==str(self.data_paths_local[idx].parts[-3])]["age"].values[0]
        except FileNotFoundError:
            eeg = np.load(self.data_paths_remote[idx])
            age = self.subj_df[self.subj_df["id"]==str(self.data_paths_remote[idx].parts[-3])]["age"].values[0]
            if self.do_save:
                self.data_paths_local[idx].parent.mkdir(parents=True, exist_ok=True)
                np.save(self.data_paths_local[idx], eeg)

        
        # age = self.ages[idx]

        if self.transforms:
            eeg = self.transforms(eeg)
        if self.target_transforms:
            age = self.target_transforms(age)

        return eeg, age

    def get_data_paths_local(self):
        data_paths_local = []
        for fpath in self.data_paths_remote:
            remote_dir = fpath
            for _ in range(3):
                remote_dir = remote_dir.parent
            data_paths_local.append(self.local_dir / fpath.relative_to(remote_dir))
        return data_paths_local
    
   
class BrainAgeTestDataset(BrainAgeDataset):

    def __init__(self, data_paths, transforms=None, local_dir=None, do_save=True):
        
        self.data_paths_remote = data_paths
        self.transforms = transforms

        self.do_save = do_save
        if local_dir:
            self.local_dir = local_dir
        else:
            self.local_dir = pathlib.Path().absolute() # current working dir
        self.data_paths_local = self.get_data_paths_local()

    def __getitem__(self, idx):

        try:
            eeg = np.load(self.data_paths_local[idx])
        except FileNotFoundError:
            eeg = np.load(self.data_paths_remote[idx])
            if self.do_save:
                self.data_paths_local[idx].parent.mkdir(parents=True, exist_ok=True)
                np.save(self.data_paths_local[idx], eeg)

        if self.transforms:
            eeg = self.transforms(eeg)

        return eeg
    
def get_ages(subj_df, data_paths):
    ages = []
    for path in data_paths:

        subj_id = path.parts[-3]
        
        # try:
        #     subj_id = int(subj_id)
        # except ValueError:
        #     pass

        try:
            age = subj_df[subj_df["id"]==subj_id]["age"].values[0]
        except IndexError:
            print(f"not found: {subj_id}")
            continue
        ages.append(age)
    return ages
    
####Transforms####

def _toimshape(x):
    return x.unsqueeze(0)

def _randomcrop(x, length):
    idx = torch.randint(0, x.shape[-1]-length, (1,))
    return x[:, idx:idx+length]

def _labelnorm(y, mean_age, std_age):
    return (y - mean_age)/std_age

def _channelnorm(x):
    return torch.div(x-x.mean(0), x.std(0))

def _cuttolen1900(x):
    return torch.split(x, 1901, dim=-1)[0]

def _padtolen1900(x):
    return torch.nn.functional.pad(x, (0, 1901-x.shape[-1])).float()

def _cuttolen700(x):
    return torch.split(x, 701, dim=-1)[0]

def _padtolen700(x):
    if x.shape[-1] <= 700:
        return torch.nn.functional.pad(x, (0, 701-x.shape[-1])).float()
    else:
        return x

def _totensor(x):
    return torch.tensor(x).float()

def _imagenorm(x):
    return torch.div(x - x.mean(dim=(-2, -1)), x.std(dim=(-2, -1)))

def composed_transform(x, transforms):
    for transform in transforms:
        x = transform(x)
    return x

